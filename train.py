import timeit
import random
import os
import sys
import argparse
import shutil
import numpy as np

import torch
import torch.nn as nn
import torch.backends.cudnn as cudnn
from torch.utils import data
import torch.optim as optim
import torchvision.transforms as transforms
from tensorboardX import SummaryWriter

import models
import dataprocess as datasets
import split_train_val


parser = argparse.ArgumentParser(description='AQI')

parser.add_argument('--dataset-root', default='./dataset/nwnu_AQI/', type=str,
                    help='Dataset root directory path')
parser.add_argument('--snapshot-dir', default='./checkpoint', type=str,
                    help='checkpoint name')
parser.add_argument('-e', '--evaluate', dest='evaluate', action='store_true',
                    help='evaluate model on validation set')
parser.add_argument('--epochs', default=300, type=int, metavar='N',
                    help='number of total epochs to run')
parser.add_argument('--start-epoch', default=0, type=int, metavar='N',
                    help='manual epoch number (useful on restarts)')
parser.add_argument('--batch-size', default=32, type=int,
                    help='Batch size for training')
parser.add_argument('--image-size', default=256, type=int,
                    help='images size for training')
parser.add_argument('--arch', metavar='ARCH', default='resnet18',
                    help='model architecture (default: resnet18)')
parser.add_argument('--pretrained', default=True,
                    help='use pre-trained model')
parser.add_argument('--seed', default=1, type=int,
                    help='Random seed to have reproducible results.')
parser.add_argument('--workers', default=0, type=int, metavar='N',
                    help='number of data loading workers (default: 4)')
parser.add_argument("--momentum", type=float, default=0.9,
                    help="Momentum component of the optimiser.")
parser.add_argument('--wd', '--weight-decay', default=1e-4, type=float,
                    metavar='W', help='weight decay (default: 1e-4)',
                    dest='weight_decay')
parser.add_argument('--resume', default='model_best.pth.tar', type=str, metavar='PATH',
                    help='path to latest checkpoint (default: none)')
parser.add_argument('--cuda', default=True,
                    help='Use CUDA to train model')
parser.add_argument('--gpu', default=3, type=int,
                    help='GPU id to use.')
parser.add_argument("--learning-rate", type=float, default=1e-2,
                    help="Base learning rate for training with polynomial decay.")

args = parser.parse_args()

best_acc = 0
classes = 0

if torch.cuda.is_available():
    if args.cuda:
        torch.set_default_tensor_type('torch.cuda.FloatTensor')
    if not args.cuda:
        print("WARNING: It looks like you have a CUDA device, but aren't " +
              "using CUDA.\nRun with --cuda for optimal training speed.")
        torch.set_default_tensor_type('torch.FloatTensor')
else:
    torch.set_default_tensor_type('torch.FloatTensor')


def main():
    global best_acc, classes

    print("Input arguments:")
    for key, val in vars(args).items():
        print("{:16} {}".format(key, val))

    # 为CPU设置种子用于生成随机数, 以使得结果是确定的
    random.seed(args.seed)
    torch.manual_seed(args.seed)
    torch.cuda.manual_seed(args.seed)

    # create checkpoint dir
    if not os.path.exists(args.snapshot_dir):
        os.makedirs(args.snapshot_dir)

    writer = SummaryWriter(args.snapshot_dir)
    # os.environ["CUDA_VISIBLE_DEVICES"] = str(args.gpu)

    # Data loading code
    train_dir = os.path.join(args.dataset_root, 'train')
    val_dir = os.path.join(args.dataset_root, 'val')

    image_size = args.image_size

    train_dataset = datasets.ImageFolder(
        train_dir,
        transforms.Compose([
            # transforms.Resize(image_size * 4),
            transforms.RandomResizedCrop(image_size),
            transforms.RandomHorizontalFlip(),
            transforms.RandomRotation(0, 360),
            transforms.ToTensor(),
        ]))

    val_dataset = datasets.ImageFolder(
        val_dir,
        transforms.Compose([
            transforms.Resize(image_size),
            transforms.CenterCrop(image_size),
            transforms.ToTensor(),
        ])
    )

    print('val_class_to_idx:   ', val_dataset.class_to_idx)
    print('train_class_to_idx: ', train_dataset.class_to_idx)
    classes = len(train_dataset.classes)

    print('classes:', classes)

    train_loader = torch.utils.data.DataLoader(
        train_dataset, batch_size=args.batch_size, shuffle=True, num_workers=args.workers)

    val_loader = torch.utils.data.DataLoader(
        val_dataset, batch_size=args.batch_size, shuffle=False, num_workers=args.workers)

    print('train_len: ', len(train_loader) * args.batch_size)
    print('val_len:   ', len(val_loader) * args.batch_size)

    print('Using image size', image_size)

    # create model
    if args.pretrained:
        print("=> using pre-trained model '{}'".format(args.arch))
        network = models.__dict__[args.arch](pretrained=True, num_classes=classes)
    else:
        print("=> creating model '{}'".format(args.arch))
        network = models.__dict__[args.arch](pretrained=False, num_classes=classes)

    # network = models.resnet101(pretrained=True, num_classes=classes)
    torch.cuda.set_device(args.gpu)
    network.cuda(args.gpu)

    # Loss function
    criterion = nn.CrossEntropyLoss().cuda(args.gpu)
    optimizer = optim.SGD(
        [{'params': filter(lambda p: p.requires_grad, network.parameters()), 'lr': args.learning_rate}],
        lr=args.learning_rate, momentum=args.momentum, weight_decay=args.weight_decay)
    optimizer.zero_grad()

    # optionally resume from a checkpoint
    if args.resume:
        if os.path.isfile(args.resume):
            print("==> loading checkpoint '{}'".format(args.resume))
            checkpoint = torch.load(os.path.join(args.resume))
            args.start_epoch = checkpoint['epoch']
            best_acc = checkpoint['best_acc']
            # if args.gpu is not None:
            #     # best_acc may be from a checkpoint from a different GPU
            #     best_acc = best_acc.to(args.gpu)
            network.load_state_dict(checkpoint['state_dict'])
            optimizer.load_state_dict(checkpoint['optimizer'])
            print("==> loaded checkpoint '{}' (epoch {}) best_acc: {}"
                  .format(args.resume, checkpoint['epoch'], best_acc))
        else:
            print("==> no checkpoint found at '{}'".format(args.resume))
    # else:
    #     saved_state_dict = torch.load(os.path.join(args.snapshot_dir, args.init_weight_file))
    #     network.load_state_dict(saved_state_dict, strict=False)

    cudnn.benchmark = True

    if args.evaluate:
        res, _ = validate(network, val_loader, criterion)
        with open('res.txt', 'w') as f:
            print(res, file=f)
        return

    # Start train
    for epoch in range(args.start_epoch, args.epochs):
        optimizer.zero_grad()
        adjust_learning_rate(optimizer, epoch)
        lr = optimizer.param_groups[0]['lr']
        print('learning_rate: {}'.format(lr))
        writer.add_scalar('learning_rate', lr, epoch)

        # train for one epoch
        start = timeit.default_timer()
        train(network, train_loader, criterion, optimizer, epoch)
        end = timeit.default_timer()
        print('Epoch: ', epoch, 'times: ', end - start, 'seconds')

        if epoch % 5 == 0:
            # evaluate on validation set
            acc, loss = validate(network, val_loader, criterion)

            print('Epoch = {}, accuracy = {}, loss = {}'.format(epoch + 1, acc, loss.item()))

            writer.add_scalar('val_loss', loss, epoch + 1)
            writer.add_scalar('val_acc', acc, epoch + 1)

            # remember best acc and save checkpoint
            is_best = acc > best_acc
            best_acc = max(acc, best_acc)

            save_checkpoint({
                'epoch': epoch + 1,
                'arch': args.arch,
                'state_dict': network.state_dict(),
                'best_acc': best_acc,
                'optimizer': optimizer.state_dict(),
            }, is_best, filename=os.path.join(args.snapshot_dir, 'checkpoint_18_no.pth.tar'))  # 'checkpoint_' + str(epoch + 1) + '.pth.tar'

    print('best_acc:  ', best_acc)


def train(network, train_loader, criterion, optimizer, epoch):
    network.train()

    total_iter = len(train_loader) * args.epochs

    for i, (images, labels) in enumerate(train_loader):

        if args.gpu is not None:
            images = images.cuda(args.gpu, non_blocking=True)
            labels = labels.cuda(args.gpu, non_blocking=True)

        # compute output
        preds = network(images)
        loss = criterion(preds, labels)

        # compute gradient and do SGD step
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if i % 10 == 0:
            acc = accuracy(preds, labels, topk=(1,))
            print('iter = {} of {} completed, loss = {}, accuracy = {}'
                  .format(1+i+epoch*len(train_loader), total_iter, loss.item() / images.size(0), acc))


def validate(network, val_loader, criterion):
    network.eval()

    with torch.no_grad():

        all_acc = []
        losses = []

        for i, (images, labels) in enumerate(val_loader):
            if args.gpu is not None:
                images = images.cuda(args.gpu, non_blocking=True)
                labels = labels.cuda(args.gpu, non_blocking=True)

            # compute output
            output = network(images)
            loss = criterion(output, labels)

            # measure accuracy and record loss
            acc = accuracy(output, labels, topk=(1,))
            all_acc.append(acc[0].cpu().numpy()[0])
            losses.append(loss.item() / images.size(0))

    return np.mean(all_acc), np.mean(losses)


def adjust_learning_rate(optimizer, epoch):
    """Sets the learning rate to the initial LR decayed by 10 every 30 epochs"""
    lr = args.learning_rate * (0.1 ** (epoch // (args.epochs//3)))
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr
    


def save_checkpoint(state, is_best, filename='checkpoint.pth.tar'):
    torch.save(state, filename)
    if is_best:
        shutil.copyfile(filename, 'model_best_18_no.pth.tar')


def accuracy(output, labels, topk=(1,)):
    """Computes the accuracy over the k top predictions for the specified values of k"""
    with torch.no_grad():
        maxk = max(topk)
        batch_size = labels.size(0)

        _, pred = output.topk(maxk, 1, True, True)
        pred = pred.t()
        correct = pred.eq(labels.view(1, -1).expand_as(pred))

        res = []
        for k in topk:
            correct_k = correct[:k].view(-1).float().sum(0, keepdim=True)
            res.append(correct_k.mul_(100.0 / batch_size))
        return res


if __name__ == '__main__':
    # dataset = 'nwnu_AQI'
    # split_train_val.dataseg(dataset)
    main()
